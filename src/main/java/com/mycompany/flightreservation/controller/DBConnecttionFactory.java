/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flightreservation.controller;

/**
 *
 * @author Room107
 */
public class DBConnecttionFactory {
   
    public DBConnection createDBConnection(String brand,String host,String dbName,String user,String password,String port){
        if(brand.equalsIgnoreCase("mysql")){
            return new MySQLDBConnecttion(host,dbName,user,password,port);
        }else{
            return null; 
        }
    } 
   
}
