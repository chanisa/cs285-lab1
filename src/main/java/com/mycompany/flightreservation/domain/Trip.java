/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.flightreservation.domain;

import java.time.ZonedDateTime;

/**
 *
 * @author Room107
 */
public class Trip {
    private AirPort fromAirport;
    private AirPort toAirport;
    private byte noOfPassengers;
    private ZonedDateTime departureDateTime;
    private ZonedDateTime returnDateTime;
    private boolean directFlinght;
    private TravelClass travelClass;
    private TripType triptype;
    
    public AirPort getfromAirport(){
        return fromAirport;
    }
    public AirPort gettoAirport(){
        return toAirport;
    }
    public byte getnoOfPassengers(){
        return noOfPassengers;
    }
    public ZonedDateTime getdepartureDateTime(){
        return departureDateTime; 
    }
    public ZonedDateTime getreturnDateTime(){
        return returnDateTime;
    }
    public boolean getdirectFlinght(){
        return directFlinght;
    }
    public TravelClass gettravelClass(){
        return travelClass;
    }    
    public TripType gettriptype(){
        return triptype;
    }
    public void setTrip(AirPort fromAirport, 
            AirPort toAirport,
            byte noOfPassengers,
            boolean directFlinght,
            TravelClass travelClass,
            TripType triptype){
        this.fromAirport = fromAirport;
        this.toAirport = toAirport;
        this.noOfPassengers = noOfPassengers;
        this.departureDateTime = departureDateTime;
        this.returnDateTime = returnDateTime;
        this.directFlinght = directFlinght;
        this.travelClass = travelClass;
        this.triptype = triptype;
    }
}
